
## 0.0.6 [04-15-2020]

* Updated IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!6

---

## 0.0.5 [04-09-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/aws-vpc-creation!4

---

## 0.0.4 [03-24-2020]

* Add readme and images

See merge request itentialopensource/pre-built-automations/staging/aws-vpc-creation!2

---

## 0.0.3 [03-24-2020]

* Add readme and images

See merge request itentialopensource/pre-built-automations/staging/aws-vpc-creation!2

---

## 0.0.2 [03-24-2020]

* Add readme and images

See merge request itentialopensource/pre-built-automations/staging/aws-vpc-creation!2

---
