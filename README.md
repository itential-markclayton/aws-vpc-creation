<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your artifact name -->
# AWS VPC Creation

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
  * [Subnets and Tags](#subnets-and-tags)
* [Components](#components)
* [Metrics](#metrics)

## Overview

The AWS VPC Creation artifact enables users of the Itential Automation Platform to leverage the power of Amazon Web Services to create a virtual private cloud. The artifact allows users to select the desired size of the VPC by specifying the CIDR block size, as well as specify a list of public and private subnets to create within the VPC.

<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<table><tr><td>
  <img src="https://gitlab.com/itentialopensource/pre-built-automations/aws-vpc-creation/raw/master/images/workflow.png" alt="workflow" width="800px">
</td></tr></table>
<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 2 min.

## Installation Prerequisites

Users must satisfy the following prerequisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2019.3.2`
* App-Artifacts
  * `^4.0.0`
* EC2 Adapter
  * `^0.6.0`

## Requirements

This artifact requires the following:

<!-- Unordered list highlighting the requirements of the artifact -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->
* Access to an Amazon Web Services account.
* EC2 Adapter instance for the region within which to create the VPC.

## Features

The main benefits and features of the artifact are outlined below.

<!-- Unordered list highlighting the most exciting features of the artifact -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->
* Allows creation of a single custom sized VPC, with an allowable CIDR block size of 16 / 24 / 28. This allows for a total address space size of 65536, 256 and 16 IP addresses, respectively.
* Enables creation of multiple subnets within the VPC. For each subnet the user may select a CIDR block size, availability zone, and designate the subnet as public or private. Public subnets will be connected to an internet gateway.
* Provides error handling for the failed creation of subnets. This gives the user an opportunity to adjust the CIDR block size and availability zone, and retry.
* Provides support for tagging all created resources with custom tags.
* Shows the pre/post diffs of information gathered from all VPCs in a selected region.
* Leverages AWS EC2 API, without any external dependencies like Ansible or Terraform.

## Future Enhancements

<!-- OPTIONAL - Mention if the artifact will be enhanced with additional features on the road map -->
<!-- Ex.: This artifact would support Cisco XR and F5 devices -->
In the future, this artifact may be expanded to allow the creation of multiple VPCs within multiple regions.

## How to Install

To install the artifact:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the artifact. If you do not currently have App-Artifacts installed on your server, please download the installer from your Nexus repository. Refer to the install instructions included in the App-Artifacts README.
* The artifact can be installed from within App-Artifacts. Simply search for the name of your desired artifact and click the install button (as shown below).

<!-- REPLACE BELOW WITH IMAGE OF YOUR PUBLISHED ARTIFACT -->
<table><tr><td>
  <img src="https://gitlab.com/itentialopensource/pre-built-automations/aws-vpc-creation/raw/master/images/install.png" alt="install" width="600px">
</td></tr></table>
<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED ARTIFACT -->

* Alternatively, you may clone this repository and run `npm pack` to create a tarball which can then be installed via the offline installer in App-Artifacts. Consult the documentation for App-Artifacts for further information.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the artifact:

<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->
* This artifact may be run from Automation Catalog by clicking `Run` for the AWS VPC Creation item. The user will be presented with a form to select options to customize the VPC that will be created.
* Select the EC2 Adapter instance to use with the automation. The adapter instance that is selected will determine the AWS region within which the VPC will be created.
* Specify the first IPv4 address for the range of addresses to associate with the VPC.
* Select the CIRD block size, which may be one of three values:

    * `16`: 65,536 total addresses
    * `24`: 256 total addresses
    * `28`: 16 total addresses

* The number of available addresses may be smaller than listed above, as AWS reserves some addresses for internal use.

<table><tr><td>
  <img src="https://gitlab.com/itentialopensource/pre-built-automations/aws-vpc-creation/raw/master/images/ac-form.png" alt="json-form" width="600px">
</td></tr></table>

### Subnets and Tags

#### Subnets

To create a subnet, click the `+` button (bottom right-hand corner) of the Subnets table.

<table><tr><td>
  <img src="https://gitlab.com/itentialopensource/pre-built-automations/aws-vpc-creation/raw/master/images/subnets.png" alt="subnets" width="600px">
</td></tr></table>

For each subnet, specify the CIDR block size. The CIDR block size may be one of three values:

* `16`: 65,536 total addresses
* `24`: 256 total addresses
* `28`: 16 total addresses

Of note, the total number of addresses allocated to the VPC cannot be exceeded. For example, if the VPC has a CIDR block size of 16, then only a single subnet with a CIDR block size of 16 may be created. The created subnets will be allocated and arranged so that the address spaces do not overlap.

Select whether the subnet is to be public or private. Public subnets will be associated with a route table attached to an internet gateway, while private subnets will be associated with the default route table for the VPC.

Optionally, an availability zone may be provided for each subnet. Check the availability zone offerings for the region in which the VPC is created.

#### Tags

To add a tag, click the `+` button (bottom right-hand corner) of the Tags table.

<table><tr><td>
  <img src="https://gitlab.com/itentialopensource/pre-built-automations/aws-vpc-creation/raw/master/images/tags.png" alt="tags" width="600px">
</td></tr></table>

For each tag, specify a key-value pair. Tags will be added to all created resources, which includes:

* VPC
* Subnets
* Route tables
* Internet Gateway

## Components

The workflow for creating a VPC is broken up into child workflows for each stage of the creation process after the VPC itself has been created.

### Create Subnets

This workflow will create the specified public and private subnets. The address spaces for all subnets will be allocated so as not to overlap based upon the CIDR block size.

#### Error Handling

If a subnet fails to be created, the user will be presented with a form pre-populated with the CIDR block size and availability zone originally specified. The user may choose to either change the CIDR block size and / or availability zone to retry, or simply ignore the error to continue with the automation.

<table><tr><td>
  <img src="https://gitlab.com/itentialopensource/pre-built-automations/aws-vpc-creation/raw/master/images/subnets-error.png" alt="subnets-error" width="600px">
</td></tr></table>

<table><tr><td>
  <img src="https://gitlab.com/itentialopensource/pre-built-automations/aws-vpc-creation/raw/master/images/subnets-form.png" alt="subnets-form" width="400px">
</td></tr></table>

### Create Public Route Table

This workflow will create a new route table which will be associated with all created public subnets.

### Create Internet Gateway

This workflow will attach an internet gateway to the previously created route table. This will allow all designated public subnets to be reachable outside of the VPC over the internet.

### Create Tags

This workflow will tag _all_ resources created throughout the automation with the provided tags.

## Metrics

When the automation has finished running, the user will be provided with a diff that displays information about all VPCs in the region.

<table><tr><td>
  <img src="https://gitlab.com/itentialopensource/pre-built-automations/aws-vpc-creation/raw/master/images/diffs.png" alt="diffs" width="600px">
</td></tr></table>
